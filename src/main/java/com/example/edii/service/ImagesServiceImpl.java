package com.example.edii.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.edii.assembler.ImagesAssembler;
import com.example.edii.model.dto.InputDto2;
import com.example.edii.model.entity.ImagesEntity;
import com.example.edii.repository.ImageRepo;

@Service
@Transactional
public class ImagesServiceImpl implements ImagesService {
	@Autowired
	private ImageRepo imgRepo;
	@Autowired
	private ImagesAssembler imgAss;
	
	@Override
	public String insertData(InputDto2 dto) {
		ImagesEntity entity=imgRepo.save(imgAss.fromDto(dto));
		imgRepo.save(entity);
		String response=("success");
		return response;
	}
	
	@Override
	public String deleteData(String idReqBook) {
		ImagesEntity entity=imgRepo.findById(idReqBook).get();
		imgRepo.deleteById(entity.getIdReqBook());
		String response=("success");
		return response;
	}
	
	@Override
	public String updateData(InputDto2 dto, String idReqBook) {
		ImagesEntity entity=imgRepo.findById(idReqBook).get();
		imgRepo.save(entity);
		String response=("success");
		return response;
	}
}
