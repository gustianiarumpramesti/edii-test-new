package com.example.edii.service;

import com.example.edii.model.dto.InputDto1;

public interface LogService {
	String updateData(InputDto1 newInput, String idReqBook);
	String insertData(InputDto1 dto);
	String deleteData(String idReqBook);
	
}
