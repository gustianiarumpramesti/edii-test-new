package com.example.edii.service;

import com.example.edii.model.dto.InputDto2;

public interface ImagesService {
	String insertData(InputDto2 dto);
	String updateData (InputDto2 dto, String idReqBook);
	String deleteData(String idReqBook);

}
