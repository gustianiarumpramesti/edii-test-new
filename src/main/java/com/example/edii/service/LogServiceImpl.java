package com.example.edii.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.edii.assembler.LogsAssembler;
import com.example.edii.model.entity.LogEntity;
import com.example.edii.model.dto.InputDto1;
import com.example.edii.repository.LogRepo;

@Service
@Transactional
public class LogServiceImpl implements LogService {
	@Autowired
	private LogRepo logRepo;
	@Autowired
	private LogsAssembler logAss;
	
	@Override
	public String insertData(InputDto1 dto) {
		LogEntity entity=logRepo.save(logAss.fromDto(dto));
		logRepo.save(entity);
		String response=("success");
		return response;
	}
	
	@Override
	public String deleteData(String idReqBook) {
		LogEntity entity=logRepo.findById(idReqBook).get();
		logRepo.deleteById(entity.getIdReqBook());
		String response=("success");
		return response;
	}
	
	@Override
	public String updateData(InputDto1 dto, String idReqBook) {
		LogEntity entity=logRepo.findById(idReqBook).get();
		logRepo.save(entity);
		String response=("success");
		return response;
	}

}
