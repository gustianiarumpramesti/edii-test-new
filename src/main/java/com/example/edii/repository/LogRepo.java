package com.example.edii.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.edii.model.entity.LogEntity;

public interface LogRepo extends JpaRepository<LogEntity, String> {

}
