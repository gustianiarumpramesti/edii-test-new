package com.example.edii.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.edii.model.entity.ImagesEntity;

public interface ImageRepo extends JpaRepository<ImagesEntity, String> {

}
