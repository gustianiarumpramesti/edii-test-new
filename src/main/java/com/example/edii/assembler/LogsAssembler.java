package com.example.edii.assembler;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.edii.model.dto.InputDto1;
import com.example.edii.model.entity.LogEntity;
import com.example.edii.repository.LogRepo;

public class LogsAssembler implements InterfaceAssembler<LogEntity, InputDto1> {
	 @Autowired
	    private LogRepo repository;

	    @Override
	    public LogEntity fromDto(InputDto1 dto) {
	        if (dto == null)
	            return null;

	        LogEntity entity = new LogEntity();
	        if (dto.getIdReqBook() != null) {
	            Optional<LogEntity> temp = this.repository.findById(dto.getIdReqBook());
	            if(temp.isPresent()){
	                entity = temp.get();
	            }
	        }

	        if (dto.getIdReqBook() != null) entity.setIdReqBook(dto.getIdReqBook());
	        if (dto.getNmPlatiform() != null) entity.setTermOfPayment(dto.getNmPlatiform());
	        if (dto.getIdPlatform() != null) entity.setIdPlatform(dto.getIdPlatform());
	        if (dto.getDocType() != null) entity.setDocType(dto.getDocType());
	        if (dto.getTermOfPayment() != null) entity.setTermOfPayment(dto.getTermOfPayment());

	        return entity;
	    }

	    @Override
	    public InputDto1 fromEntity(LogEntity entity) {
	        if (entity == null) return null;
	        return InputDto1.builder()
	                .idReqBook(entity.getIdReqBook())
	                .idPlatform(entity.getIdPlatform())
	                .nmPlatiform(entity.getNmPlatform())
	                .termOfPayment(entity.getTermOfPayment())
	                .docType(entity.getDocType())
	                .build();
	    }
}
