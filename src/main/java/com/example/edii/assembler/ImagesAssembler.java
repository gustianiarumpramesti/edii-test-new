package com.example.edii.assembler;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.edii.model.dto.InputDto2;
import com.example.edii.model.entity.ImagesEntity;
import com.example.edii.repository.ImageRepo;
import com.example.edii.repository.LogRepo;

public class ImagesAssembler implements InterfaceAssembler<ImagesEntity, InputDto2> {
	 @Autowired
	    private LogRepo logRepo;
	 @Autowired
	    private ImageRepo imgRepo;

	    @Override
	    public ImagesEntity fromDto(InputDto2 dto) {
	        if (dto == null)
	            return null;

	        ImagesEntity entity = new ImagesEntity();
	        if (dto.getIdReqBook() != null) {
	            Optional<ImagesEntity> temp = this.imgRepo.findById(dto.getIdReqBook());
	            if(temp.isPresent()){
	                entity = temp.get();
	            }
	        }

	        if (dto.getIdReqBook() != null) entity.setIdReqBook(dto.getIdReqBook());
	        if (dto.getDesc() != null) entity.setDesc(dto.getDesc());
	        
	        
	        return entity;
	    }

	    @Override
	    public InputDto2 fromEntity(ImagesEntity entity) {
	        if (entity == null) return null;
	        if (entity.getIdReqBook() != null && entity.getIdReqBook() == null)
	            entity.setLog(logRepo.findById(entity.getIdReqBook()).get());
	        return InputDto2.builder()
	                .idReqBook(entity.getLog().getIdReqBook())
	                .desc(entity.getDesc())
	                .build();
	    }
}
