package com.example.edii.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InputDto1 {
	 private String idReqBook;
	 private String idPlatform;
	 private String nmPlatiform;
	 private String docType;
	 private String termOfPayment;
}
