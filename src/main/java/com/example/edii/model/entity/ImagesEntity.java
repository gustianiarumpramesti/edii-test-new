package com.example.edii.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="images_nle_api01")
public class ImagesEntity {
	@OneToOne
	@JoinColumn(name="idrequestbooking", unique=true, nullable=false)
	private LogEntity log;
	
	@Id
	@Column(name="idrequestbooking")
	private String idReqBook;
	
	@Column(name="description")
	private byte[] desc;

}
